Connect Four AI Project
====================
Nicholas Schafhauser, Richard Covatto, Samuel Kenney

Grove City College 2018

Summary
====================
This C# program allows a user to choose to play the classic game of Connect Four in three different modes. 

* Human vs. Human

* Human vs. AI

* AI vs. AI

The Artificial Intellence of this game used Alpha-Beta pruning with Minimax to find the best possible 
world to beat the other player. The heuristic that was used for the game was taken from the research of 
Jenny Lam from her paper "Heuristics in the game of Connect-K" at the Dept. of Computer Science 
UC Irvine found at [Emphasis] (http://www.jennylam.cc/assets/pdf/connectk.pdf).

How To Play
=====================
There will be a menu that is displayed to the user with the three options to play as stated above. When
an option is chosen, a Connect Four board will appear with seven buttons along the bottom to be selected when
playing in the respective column. The first player always starts with red and the second player follows with black. 

When playing Human vs. Human, the user can take alternating turns by selectingthe chosen column until 
a player wins. With Human vs. AI, the user will always begin as red with the AI playing black. The user then can 
start the game and then select a column to play after the AI has played. AI vs. AI will run in 
the background until an AI player has won. 


