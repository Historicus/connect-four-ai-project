﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{

    struct Threat
    {
        public Coordinate pos;
        public string color;
        public string oddOrEven;
    };

    class Heuristic
    {
        // board object to hold the information on the board
        private String[][] board = new String[7][];

        // List that holds threats for that game state
        List<Threat> threats = new List<Threat>();

        string winner = "";

        String whosTurn = "";

        bool isSeriousThreat = false;

        // Constructor for Heuristic class
        // Takes in the current board as its only argument
        public Heuristic(String[][] board, String whosTurn)
        {
            this.board = board;
            this.whosTurn = whosTurn;
        }

        // Heuristic taken from http://www.jennylam.cc/assets/pdf/connectk.pdf
        public double CalculateHeuristic()
        {
            double heuristic = 0.0;

            // Step 1. calculate # winning rows for p1 (red)
            heuristic += (CalculateWinningLines("Red") * 0.1);

            // Step 2. calculate # winning lines for p2 (black)
            heuristic += (CalculateWinningLines("Black") * -0.1);

            // Step 3. calculate p1 (red) odd threats
            heuristic += (CalculateThreats("Red", "Odd") * 2.0);

            // Step 4. calculate p2 (black) odd threats
            heuristic += (CalculateThreats("Black", "Odd") * -2.0);

            // Step 5. calculate p1 (red) even threats
            heuristic += (CalculateThreats("Red", "Even") * 2.0);

            // Step 6. calculate p2 (black) even threats
            heuristic += (CalculateThreats("Black", "Even") * -2.0);

            // Step 7. Check if there is a very serious threat that exists in this game state
            if (isSeriousThreat)
            {
                if (whosTurn == "Black")
                {
                    heuristic = Double.MaxValue;
                }
                else if (whosTurn == "Red")
                {
                    heuristic = Double.MinValue;
                }
            }

            // Step 8. See if p1 (red) or p2 (black) won in this game state
            if (winner == "Red")
            {
                heuristic = Double.MaxValue;
                //heuristic = Double.MinValue;
            }
            else if (winner == "Black")
            {
                //heuristic = Double.MaxValue;
                heuristic = Double.MinValue;
            }

            

            //Debug.Print(heuristic.ToString());

            //Debug.Print("-------------------------");

            return heuristic;
        }

        // Returns number of winning lines for the target color
        // Counts diagonal (left and right), rows, and columns
        private int CalculateWinningLines(string targetColor)
        {
            int count = 0;

            // Step 1a. 
            // bottom half left diagonals
            count += LeftDiagonalBottomCount(targetColor);

            // Step 1b.
            // top half left diagonals
            count += LeftDiagonalTopCount(targetColor);

            // Step 1c.
            // bottom half right diagonals
            count += RightDiagonalTopCount(targetColor);

            // Step 1d.
            // top half right diagonals
            count += RightDiagonalBottomCount(targetColor);

            // Step 1e
            // Count column
            count += ColumnCount(targetColor);

            // Step 1f
            // row count
            count += RowCount(targetColor);

            return count;
        }

        // Returns a string that is set to the value of whatever the unique color in that line is
        // "Red" "Black" "Both" or "Empty"
        // Starts at the starting position and works its way down
        // direction is which way the line is going (horiziontal, vertical, left-diagonal, right-diagonal)
        private string OnlyOneTypeOfColorInThisLine(Coordinate startOfLine, string direction)
        {
            bool containsRed = false;
            bool containsBlack = false;
            int numRed = 0, numBlack = 0, numEmpty = 0;
            Coordinate emptyPos = new Coordinate
            {
                column = -1,
                row = -1
            };
            switch (direction)
            {
                case "left-diagonal":
                    // Work our way down and to the right 4 squares
                    // Set bools to true if we encounter a red stone or a black stone
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column + i][startOfLine.row - i] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column + i][startOfLine.row - i] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column + i;
                            emptyPos.row = startOfLine.row - i;
                        }
                    }
                    break;
                case "right-diagonal":
                    // Work our way up and to the right 4 squares
                    // Set bools to true if we encounter a red stone or a black stone
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column + i][startOfLine.row + i] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column + i][startOfLine.row + i] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column + i;
                            emptyPos.row = startOfLine.row + i;
                        }
                    }
                    break;
                case "column":
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column][startOfLine.row - i] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column][startOfLine.row - i] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column;
                            emptyPos.row = startOfLine.row - i;
                        }
                    }
                    break;
                case "row":
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column + i][startOfLine.row] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column + i][startOfLine.row] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column + i;
                            emptyPos.row = startOfLine.row;
                        }
                    }
                    break;
                default:
                    Debug.Print("OnlyOneTypeOfColorInThisLine :: default case executed");
                    return "Empty";
            }

            // Calculate number of threats using numRed, numBlack, and numEmpty
            if(numEmpty == 1)
            {
                if(numRed == 3)
                {
                    // Then we have a threat for red
                    //Debug.Print("threat");
                    AddThreat(emptyPos, "Red");
                }
                else if(numBlack == 3)
                {
                    AddThreat(emptyPos, "Black");
                }
            }

            // See if somebody won in this game state
            if (numEmpty == 0)
            {
                if (numRed == 4)
                {
                    // Then red has won in this game state
                    winner =  "Red";
                }
                else if (numBlack == 4)
                {
                    // Then black has won in this game state
                    winner =  "Black";
                }
            }

            // If both bools are set to true then this line does not contain a unique color. return false.
            if (containsBlack)
            {
                if (containsRed)
                {
                    return "Both";
                }
                return "Black";
            }
            else if (containsRed)
            {
                return "Red";
            }
          
            return "Empty";
        }

        private void AddThreat(Coordinate posOfEmpty, string threatColor)
        {
            string threatType = "";
            // Calculate if it is odd or even row
            if (((posOfEmpty.row+1) % 2) == 0)
            {
                //Even threat
                threatType = "Even";
            }
            else
            {
                threatType = "Odd";
            }

            Threat threat = new Threat
            {
                pos = posOfEmpty,
                oddOrEven = threatType,
                color = threatColor
            };
            bool alreadyAdded = false;
            // Make sure we haven't already account for this threat before we add it
            foreach (Threat threatInList in threats) {
                if ((threatInList.pos.column == threat.pos.column) && (threatInList.pos.row == threat.pos.row))
                {
                    if (threatInList.color == threat.color)
                    {
                        alreadyAdded = true;
                    }
                }
            }

            // Make sure it is not a useless threat
            // A useless threat is one that can be blocked on the next move
            // is either on the bottom row or has a stone in the row directly below it
            bool isUselessThreat = false;
            //if (threat.pos.row == 0 || board[threat.pos.column][threat.pos.row - 1] != "Empty")
            //{
            //    isUselessThreat = true;
            //}

            //Check if the threat is a serious threat. A serious threat is if it is currently black's turn and red has a threat
            if (threat.pos.row == 0 || board[threat.pos.column][threat.pos.row - 1] != "Empty")
            {
                if(threat.color != whosTurn)
                {
                    // This is a very serious threat. This is not a world that we want to end up in
                    isSeriousThreat = true;
                }
            }

                if (alreadyAdded == false && isUselessThreat == false)
            {
                threats.Add(threat);
            }
        }

        // Returns number of winning rows for the target color in the top-left to bottom-right diagonal
        private int LeftDiagonalBottomCount(string targetColor)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            int count = 0;
            for (int rowStart = rowMax; rowStart > 2; rowStart--)
            {
                int curRow, col;
                for (curRow = rowStart, col = 0; curRow <= rowMax && curRow >= 0 && col < colMax; curRow--, col++)
                {
                    if ((curRow - 2) > 0)
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        if (OnlyOneTypeOfColorInThisLine(startingPos, "left-diagonal") == targetColor)
                        {
                            count++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return count;
        }

        // Returns number of winning rows for the target color in the top half diagonal
        private int LeftDiagonalTopCount(string targetColor)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            int count = 0;
            int rowStart = rowMax;
            for (int colStart = 1; colStart <= colMax - 3; colStart++)
            {
                int curRow, col;
                for (curRow = rowStart, col = colStart; curRow <= rowMax && col < colMax && curRow >= 0; curRow--, col++)
                {
                    if ((curRow - 2) > 0 && (col + 3 <= colMax))
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        if (OnlyOneTypeOfColorInThisLine(startingPos, "left-diagonal") == targetColor)
                        {
                            count++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return count;
        }

        // Returns number of winning rows for the target color in the top right diagonals
        private int RightDiagonalTopCount(string targetColor)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            int count = 0;
            for (int rowStart = 0; rowStart <= 2; rowStart++)
            {
                int curRow, col;
                for (curRow = rowStart, col = 0; curRow <= rowMax && curRow >= 0 && col < colMax; curRow++, col++)
                {
                    if ((curRow + 2) < rowMax)
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        if (OnlyOneTypeOfColorInThisLine(startingPos, "right-diagonal") == targetColor)
                        {
                            count++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return count;
        }

        // Returns number of winning rows for the target color in the bottom half right diagonal
        private int RightDiagonalBottomCount(string targetColor)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            int count = 0;
            int rowStart = 0;
            for (int colStart = 1; colStart <= colMax - 3; colStart++)
            {
                int curRow, col;
                for (curRow = rowStart, col = colStart; curRow <= rowMax && col < colMax && curRow >= 0; curRow++, col++)
                {
                    if ((curRow + 2) < rowMax && (col + 3 <= colMax))
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        if (OnlyOneTypeOfColorInThisLine(startingPos, "right-diagonal") == targetColor)
                        {
                            count++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return count;
        }

        // Returns number of winning rows for the target color in the specified column
        private int ColumnCount(string targetColor)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            int count = 0;
            
            for(int curCol = 0; curCol <= colMax; curCol++)
            {
                for(int curRow = rowMax; curRow > 2; curRow--)
                {
                    //Debug.Print(curCol + " " + curRow);
                    Coordinate startingPos = new Coordinate
                    {
                        column = curCol,
                        row = curRow
                    };
                    if (OnlyOneTypeOfColorInThisLine(startingPos, "column") == targetColor)
                    {
                        count++;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return count;
        }

        // Returns number of winning rows for the target color in the specified row
        private int RowCount(string targetColor)
        {
            int rowMax = 5; // index at 0
            int count = 0;

            for (int curRow = 0; curRow <= rowMax; curRow++)
            {
                for (int curCol = 0; curCol < 4; curCol++)
                {
                    //Debug.Print(curCol + " " + curRow);
                    Coordinate startingPos = new Coordinate
                    {
                        column = curCol,
                        row = curRow
                    };
                    if (OnlyOneTypeOfColorInThisLine(startingPos, "row") == targetColor)
                    {
                        count++;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return count;
        }

        // Returns the number of threats that the target color has for a specified threat type (odd or even)
        private int CalculateThreats(string targetColor, string threatType)
        {
            int count = 0;
            foreach (Threat threatInList in threats)
            {
               if (threatInList.color == targetColor && threatInList.oddOrEven == threatType)
                {
                    count++;
                }
            }
            return count;
        }
    }
}
