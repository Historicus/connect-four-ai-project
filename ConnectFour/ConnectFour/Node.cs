﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    class Node
    {
        // each node gets a heuristic
        double heuristic;
        // each node will always have seven or less children
        List<Node> children;
    }
}
