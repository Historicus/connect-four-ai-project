﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ConnectFour
{

    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
        }
        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            String option = null;
            if ((bool) hvh.IsChecked)
            {
                option = "hvh";
            }
            else if ((bool)hva.IsChecked)
            {
                option = "hva";
            }
            else
            {
                option = "ava";
            }

            MainWindow mw = new MainWindow(option);
            mw.Show();
        
            if (option.Equals("ava"))
            {
                mw.startAI();
            }

            this.Close();
        }
    }
}
