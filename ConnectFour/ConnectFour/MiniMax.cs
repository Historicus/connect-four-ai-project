﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectFour
{
    class MiniMax
    {

        struct MoveAndHeuristic
        {
            public Double heuristic; // Hueristic value of that world
            public int col; // Column that we will play a chip in (0-6)
        };

        // Constructor for MiniMax class
        public MiniMax()
        {
          
        }

        private Coordinate findCoords(String[][] OrigBoard, String[][] world)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0

            for (int col = 0; col <= colMax; col++)
            {
                for (int curRow = 0; curRow <= rowMax; curRow++)
                {
                    if(OrigBoard[col][curRow] != world[col][curRow])
                    {
                        Coordinate coord = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        return coord;
                    }
                }
            }

            Coordinate errorCoord = new Coordinate
            {
                column = -1,
                row = -1
            };

            return errorCoord;
        }

        //inspiration for alpha-beta pruning algroithm from textbook psuedocode(pg. 170)
        //will return a board
        public int AlphaBetaSearch(String[][] OrigBoard, int depth, String player)
        {
            //check to make sure depth is positive
            if (depth < 1)
            {
                depth = 1;
            }

            MoveAndHeuristic v;

            if (player == "Black") // Minimizing player
            {
                v = MinValue(OrigBoard, -1, Double.MinValue, Double.MaxValue, depth, player);
                
            }
            else // Maximizing player
            {
                v = MaxValue(OrigBoard, -1, Double.MinValue, Double.MaxValue, depth, player);
                //worlds = GenerateWorlds(OrigBoard, "Red");
            }

            return v.col;
        }

        private MoveAndHeuristic MaxValue(String[][] state, int move, double alpha, double beta, int depth, String player)
        {
            player = "Black";
            MoveAndHeuristic sh = new MoveAndHeuristic
            {
                heuristic = Double.MinValue,
                col = move
            };

            //check if we are at a terminal state or if we are at the depth
            string didRedOrBlackWin = DidSomeoneWin(state);
            if (depth == 0 || (didRedOrBlackWin != "Neither"))
            {
                if (didRedOrBlackWin != "Neither")
                {
                    //Debug.Print(didRedOrBlackWin);
                }
                Heuristic heuristicClass = new Heuristic(state, player);
                sh.heuristic = heuristicClass.CalculateHeuristic();
                PrintWorld(state);
                return sh;
            }
            
            List<String[][]> worlds = GenerateWorlds(state, "Red");
            
            foreach (String[][] w in worlds)
            {
                int nextMove = findCoords(state, w).column;
                MoveAndHeuristic temp = MinValue(w, nextMove, alpha, beta, depth - 1, player);
                
                // Pick the maximum heuristic and move
                if(temp.heuristic >= sh.heuristic)
                {
                    sh.heuristic = temp.heuristic;
                    sh.col = nextMove;
                }

                // V >= Beta
                if (sh.heuristic >= beta)
                {
                    return sh;
                }

                // Calculate new alpha
                alpha = Math.Max(alpha, sh.heuristic);
            }
            return sh;
        }

        private MoveAndHeuristic MinValue(String[][] state, int move, double alpha, double beta, int depth, String player)
        {
            player = "Red";
            MoveAndHeuristic sh = new MoveAndHeuristic
            {
                heuristic = Double.MaxValue,
                col = move
            };

            //check if we are at a terminal state or if we are at the depth
            string didRedOrBlackWin = DidSomeoneWin(state);
            if (depth == 0 || (didRedOrBlackWin != "Neither"))
            {
                if(didRedOrBlackWin != "Neither")
                {
                    Debug.Print(didRedOrBlackWin);
                }
                Heuristic heuristicClass = new Heuristic(state, player);
                sh.heuristic = heuristicClass.CalculateHeuristic();
                PrintWorld(state);
                return sh;
            }
            
            List<String[][]> worlds = GenerateWorlds(state, "Black");

            foreach (String[][] w in worlds)
            {
                int nextMove = findCoords(state, w).column;
                MoveAndHeuristic temp = MaxValue(w, nextMove, alpha, beta, depth - 1, player);
                
                // MIN
                if (temp.heuristic <= sh.heuristic)
                {
                    sh.heuristic = temp.heuristic;
                    sh.col = nextMove;
                }

                // V <= Alpha
                if (sh.heuristic <= alpha)
                {
                    return sh;
                }

                // Calculate new beta
                beta = Math.Min(beta, sh.heuristic);
            }
            return sh;

        }

        private void PrintWorld(String[][] world)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            for (int row = rowMax; row >= 0; row--)
            {
                string rowString = "";
                for (int col = 0; col <= colMax; col++)
                {
                    string obj = "";
                    if (world[col][row] == "Empty")
                    {
                        obj = "0";
                    }
                    else if (world[col][row] == "Red")
                    {
                        obj = "R";
                    }
                    else if (world[col][row] == "Black")
                    {
                        obj = "B";
                    }
                    rowString += obj + " ";
                }
            }
        }

        //start of nicky's
        // Takes the current state of the board and returns a List of all the possible boards that could be generated next move
        // playerColor is the color of whos turn it is
        // List is sorted based on move ordering (middle moves in front and then others later)
        private List<String[][]> GenerateWorlds(String[][] curBoard, String playerColor)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            List<int> columnMoveOrdering = new List<int> { 3, 2, 4, 1, 5, 0, 6 };
            List<String[][]> listOfGeneratedWorlds = new List<String[][]>();

            for (int curCol = 0; curCol <= colMax; curCol++)
            {
                if (curBoard[columnMoveOrdering[curCol]][rowMax] == "Empty")
                {
                    // Then that column is still playable. 
                    // Find the lowest row in that column where you can play a chip
                    // start at the bottom of the column and work our way up
                    for (int curRow = 0; curRow <= rowMax; curRow++)
                    {
                        if (curBoard[columnMoveOrdering[curCol]][curRow] == "Empty")
                        {
                            // We can play a chip in this spot.
                            // Place a chip there a save that world to our list
                            String[][] newWorld = CopyOfCurBoard(curBoard);
                            newWorld[columnMoveOrdering[curCol]][curRow] = playerColor;
                            listOfGeneratedWorlds.Add(newWorld);
                            break;
                        }
                    }
                }
            }

            // Tell Richie to make sure that the size of this list is greater than 0 before doing anything with it
            return listOfGeneratedWorlds;

        }

        private String[][] CopyOfCurBoard(String[][] curBoard)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            String[][] newWorld = new String[7][];

            // add 7 empty coloumns
            newWorld[0] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            newWorld[1] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            newWorld[2] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            newWorld[3] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            newWorld[4] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            newWorld[5] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            newWorld[6] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };

            for (int col = 0; col <= colMax; col++)
            {
                for (int row = 0; row <= rowMax; row++)
                {
                    newWorld[col][row] = String.Copy(curBoard[col][row]);
                }
            }

            return newWorld;
        }

        // returns "Red" if red won
        // "Black" if black won
        // "Neither" if no one won yet
        public String DidSomeoneWin(String[][] curBoard)
        {

            string winner = "Neither";

            // Step 1a. 
            // bottom half left diagonals
            winner = LeftDiagonalBottomCount(curBoard);
            if (winner != "Neither")
            {
                return winner;
            }

            // Step 1b.
            // top half left diagonals
            winner = LeftDiagonalTopCount(curBoard);
            if (winner != "Neither")
            {
                return winner;
            }

            // Step 1c.
            // bottom half right diagonals
            winner = RightDiagonalTopCount(curBoard);
            if (winner != "Neither")
            {
                return winner;
            }

            // Step 1d.
            // top half right diagonals
            winner = RightDiagonalBottomCount(curBoard);
            if (winner != "Neither")
            {
                return winner;
            }

            // Step 1e
            // Count column
            winner = ColumnCount(curBoard);
            if (winner != "Neither")
            {
                return winner;
            }

            // Step 1f
            // row count
            winner = RowCount(curBoard);
            if (winner != "Neither")
            {
                return winner;
            }

            return "Neither";

        }

        // Returns a string that is set to the value of whatever the unique color in that line is
        // "Red" "Black" "Both" or "Empty"
        // Starts at the starting position and works its way down
        // direction is which way the line is going (horiziontal, vertical, left-diagonal, right-diagonal)
        private string OnlyOneTypeOfColorInThisLine(Coordinate startOfLine, string direction, String[][] board)
        {
            bool containsRed = false;
            bool containsBlack = false;
            int numRed = 0, numBlack = 0, numEmpty = 0;
            Coordinate emptyPos = new Coordinate
            {
                column = -1,
                row = -1
            };
            switch (direction)
            {
                case "left-diagonal":
                    // Work our way down and to the right 4 squares
                    // Set bools to true if we encounter a red stone or a black stone
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column + i][startOfLine.row - i] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column + i][startOfLine.row - i] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column + i;
                            emptyPos.row = startOfLine.row - i;
                        }
                    }
                    break;
                case "right-diagonal":
                    // Work our way up and to the right 4 squares
                    // Set bools to true if we encounter a red stone or a black stone
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column + i][startOfLine.row + i] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column + i][startOfLine.row + i] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column + i;
                            emptyPos.row = startOfLine.row + i;
                        }
                    }
                    break;
                case "column":
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column][startOfLine.row - i] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column][startOfLine.row - i] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column;
                            emptyPos.row = startOfLine.row - i;
                        }
                    }
                    break;
                case "row":
                    for (int i = 0; i < 4; i++)
                    {
                        if (board[startOfLine.column + i][startOfLine.row] == "Red")
                        {
                            numRed++;
                            containsRed = true;
                        }
                        else if (board[startOfLine.column + i][startOfLine.row] == "Black")
                        {
                            numBlack++;
                            containsBlack = true;
                        }
                        else
                        {
                            // Then that spot must be empty
                            numEmpty++;
                            emptyPos.column = startOfLine.column + i;
                            emptyPos.row = startOfLine.row;
                        }
                    }
                    break;
                default:
                    return "Empty";
            }

            // Calculate number of threats using numRed, numBlack, and numEmpty
            if (numEmpty == 0)
            {
                if (numRed == 4)
                {
                    // Then red has won in this game state
                    return "Red";
                }
                else if (numBlack == 4)
                {
                    // Then black has won in this game state
                    return "Black";
                }
            }

            return "Neither";
        }

        // Returns number of winning rows for the target color in the top-left to bottom-right diagonal
        private string LeftDiagonalBottomCount(String[][] curBoard)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            for (int rowStart = rowMax; rowStart > 2; rowStart--)
            {
                int curRow, col;
                for (curRow = rowStart, col = 0; curRow <= rowMax && curRow >= 0 && col < colMax; curRow--, col++)
                {
                    if ((curRow - 2) > 0)
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        string winner = OnlyOneTypeOfColorInThisLine(startingPos, "left-diagonal", curBoard);
                        if (winner != "Neither")
                        {
                            return winner;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return "Neither";
        }

        // Returns number of winning rows for the target color in the top half diagonal
        private string LeftDiagonalTopCount(String[][] curBoard)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            int rowStart = rowMax;
            for (int colStart = 1; colStart <= colMax - 3; colStart++)
            {
                int curRow, col;
                for (curRow = rowStart, col = colStart; curRow <= rowMax && col < colMax && curRow >= 0; curRow--, col++)
                {
                    if ((curRow - 2) > 0 && (col + 3 <= colMax))
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        string winner = OnlyOneTypeOfColorInThisLine(startingPos, "left-diagonal", curBoard);
                        if (winner != "Neither")
                        {
                            return winner;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return "Neither";
        }

        // Returns number of winning rows for the target color in the top right diagonals
        private string RightDiagonalTopCount(String[][] curBoard)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            for (int rowStart = 0; rowStart <= 2; rowStart++)
            {
                int curRow, col;
                for (curRow = rowStart, col = 0; curRow <= rowMax && curRow >= 0 && col < colMax; curRow++, col++)
                {
                    if ((curRow + 2) < rowMax)
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        string winner = OnlyOneTypeOfColorInThisLine(startingPos, "right-diagonal", curBoard);
                        if (winner != "Neither")
                        {
                            return winner;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return "Neither";
        }

        // Returns number of winning rows for the target color in the bottom half right diagonal
        private string RightDiagonalBottomCount(String[][] curBoard)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0
            int rowStart = 0;
            for (int colStart = 1; colStart <= colMax - 3; colStart++)
            {
                int curRow, col;
                for (curRow = rowStart, col = colStart; curRow <= rowMax && col < colMax && curRow >= 0; curRow++, col++)
                {
                    if ((curRow + 2) < rowMax && (col + 3 <= colMax))
                    {
                        //Debug.Print(col + " " + curRow);
                        Coordinate startingPos = new Coordinate
                        {
                            column = col,
                            row = curRow
                        };
                        string winner = OnlyOneTypeOfColorInThisLine(startingPos, "right-diagonal", curBoard);
                        if (winner != "Neither")
                        {
                            return winner;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return "Neither";
        }

        // Returns number of winning rows for the target color in the specified column
        private string ColumnCount(String[][] curBoard)
        {
            int rowMax = 5; // index at 0
            int colMax = 6; // index at 0

            for (int curCol = 0; curCol <= colMax; curCol++)
            {
                for (int curRow = rowMax; curRow > 2; curRow--)
                {
                    //Debug.Print(curCol + " " + curRow);
                    Coordinate startingPos = new Coordinate
                    {
                        column = curCol,
                        row = curRow
                    };
                    string winner = OnlyOneTypeOfColorInThisLine(startingPos, "column", curBoard);
                    if (winner != "Neither")
                    {
                        return winner;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return "Neither";
        }

        // Returns number of winning rows for the target color in the specified row
        private string RowCount(String[][] curBoard)
        {
            int rowMax = 5; // index at 0

            for (int curRow = 0; curRow <= rowMax; curRow++)
            {
                for (int curCol = 0; curCol < 4; curCol++)
                {
                    //Debug.Print(curCol + " " + curRow);
                    Coordinate startingPos = new Coordinate
                    {
                        column = curCol,
                        row = curRow
                    };
                    string winner = OnlyOneTypeOfColorInThisLine(startingPos, "row", curBoard);
                    if (winner != "Neither")
                    {
                        return winner;
                    }
                }
            }
            //Debug.Print(count.ToString());
            return "Neither";
        }
    }
}
