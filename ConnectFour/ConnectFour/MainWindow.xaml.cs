﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Diagnostics;


namespace ConnectFour
{


    struct Coordinate
    {
        public int column;
        public int row;
    };

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int chips = 0;

        // board object to hold the information on the board
        String[][] board = new String[7][];
        List<List<Ellipse>> UIboard = new List<List<Ellipse>>();

        // These list of tuples hold the coordinates of red and black moves that have been played
        // goes [column, row]
        List<Coordinate> redMoves = new List<Coordinate>();
        List<Coordinate> blackMoves = new List<Coordinate>();

        bool redTurn = false;
        bool blackTurn = false;

        String playerOption;

        public MainWindow() { }

        public MainWindow(String playOption)
        {
            InitializeComponent();

            // set first turn to be red
            redTurn = true;

            createBoard();

            drawBoard();

            // passes which choice the user chose at the beginning
            // use this string to tell which type of game we are playing
            this.playerOption = playOption;
        }

        // adds a chip to the first column
        private void columnZeroButton_Click(object sender, RoutedEventArgs e)
        {
            if (play(0))
            {
                columnZeroButton.IsEnabled = false;
            }
        }

        // adds a chip to the second column
        private void columnOneButton_Click(object sender, RoutedEventArgs e)
        {
            if (play(1))
            {
                columnOneButton.IsEnabled = false;
            }
        }

        // adds a chip to the third column
        private void columnTwoButton_Click(object sender, RoutedEventArgs e)
        {
            if (play(2))
            {
                columnTwoButton.IsEnabled = false;
            }
        }

        // adds a chip to the fourth column
        private void columnThreeButton_Click(object sender, RoutedEventArgs e)
        {
            if (play(3))
            {
                columnThreeButton.IsEnabled = false;
            }
        }

        // adds a chip to the fifth column
        private void columnFourButton_Click(object sender, RoutedEventArgs e)
        {
            if (play(4))
            {
                columnFourButton.IsEnabled = false;
            }
        }

        // adds a chip to the sixth column
        private void columnFiveButton_Click(object sender, RoutedEventArgs e)
        {
            if (play(5))
            {
                columnFiveButton.IsEnabled = false;
            }
        }

        // adds a chip to the seventh column
        private void columnSixButton_Click(object sender, RoutedEventArgs e)
        {
            if (play(6))
            {
                columnSixButton.IsEnabled = false;
            }
        }

        public void startAI()
        {

            columnZeroButton.IsEnabled = false;
            columnOneButton.IsEnabled = false;
            columnTwoButton.IsEnabled = false;
            columnThreeButton.IsEnabled = false;
            columnFourButton.IsEnabled = false;
            columnFiveButton.IsEnabled = false;
            columnSixButton.IsEnabled = false;

            // start the game
            MiniMax miniMaxClass = new MiniMax();
            int col = miniMaxClass.AlphaBetaSearch(board, 4, "Red");

            play(col); // play the AI's choice
        }

        // when someone has won the game
        private void EndGame(String winner)
        {
            drawBoard();

            currentTurn.Visibility = Visibility.Hidden;

            columnZeroButton.IsEnabled = false;
            columnOneButton.IsEnabled = false;
            columnTwoButton.IsEnabled = false;
            columnThreeButton.IsEnabled = false;
            columnFourButton.IsEnabled = false;
            columnFiveButton.IsEnabled = false;
            columnSixButton.IsEnabled = false;

            if (winner.Equals("Red"))
            {
                endGameText.Text = "Red Won!";
                endGameText.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Red);

            } else
            {
                endGameText.Text = "Black Won!";
                endGameText.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
            }

            // show the button at the bottom
            this.Height = 725;
        }

        private void Draw()
        {
            drawBoard();

            currentTurn.Visibility = Visibility.Hidden;

            columnZeroButton.IsEnabled = false;
            columnOneButton.IsEnabled = false;
            columnTwoButton.IsEnabled = false;
            columnThreeButton.IsEnabled = false;
            columnFourButton.IsEnabled = false;
            columnFiveButton.IsEnabled = false;
            columnSixButton.IsEnabled = false;

            endGameText.Text = "Draw!";

            // show the button at the bottom
            this.Height = 725;

        }

        // set value in column where chip is played
        private bool play(int ind)
        {
            if(ind == -1)
            {
                return true;
            }

            MiniMax check = new MiniMax();

            chips++;
            bool lastSpot = false;

            if (redTurn)
            {
                currentTurn.Content = "Black's Turn";
                currentTurn.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
            } else
            {
                currentTurn.Content = "Red's Turn";
                currentTurn.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Red);
            }

            // this for loop holds true for all types
            for (int i = 0; i < board[ind].Length; i++)
            {
                if (board[ind][i].Equals("Empty"))
                {
                    if (redTurn && !blackTurn)
                    {
                        redTurn = false;
                        blackTurn = true;
                        board[ind][i] = "Red";

                        Coordinate redPos = new Coordinate
                        {
                            column = ind,
                            row = i
                        };
                        redMoves.Add(redPos);

                        if (check.DidSomeoneWin(board).Equals("Red"))
                        {
                            EndGame("Red");
                            return true;
                        }
                    }
                    else
                    {
                        redTurn = true;
                        blackTurn = false;
                        board[ind][i] = "Black";

                        Coordinate blackPos = new Coordinate
                        {
                            column = ind,
                            row = i
                        };
                        blackMoves.Add(blackPos);

                        if (check.DidSomeoneWin(board).Equals("Black"))
                        {
                            EndGame("Black");
                            return true;
                        }
                    }

                    if (i == board[ind].Length - 1) // if last turn
                    {
                        lastSpot = true;
                    }
                    break;
                }
            }

            // check after everyone has played if it is a draw, will only get here if noone has won
            if (chips == 42)
            {
                Draw();
            }

            switch (playerOption)
            {
                //================== HUMAN v. AI======================
                case "hva":
                    if (blackTurn)
                    {
                        MiniMax miniMaxClass = new MiniMax();
                        int col = miniMaxClass.AlphaBetaSearch(board, 3, "Black");

                        play(col); // play the AI's choice
                    }
                    break;
                //===================================================================
                case "ava":
                    drawBoard();
                    if (redTurn)
                    {
                        MiniMax miniMaxClass = new MiniMax();
                        int col = miniMaxClass.AlphaBetaSearch(board, 6, "Red");

                        play(col); // play the AI's choice
                    }
                    if (blackTurn)
                    {
                        MiniMax miniMaxClass = new MiniMax();
                        int col = miniMaxClass.AlphaBetaSearch(board, 2, "Black");

                        play(col); // play the AI's choice
                    }
                    break;
            }

            drawBoard();

            // if false, not last turn, if true, last turn
            return lastSpot;
        }

        // goes through backend board and draws the front end for the user
        private void drawBoard()
        {
            // draws the front end board based on what has been changed in the backend
            for (int i = 0; i < board.Length; i++)
            {
                for (int j = 0; j < board[i].Length; j++)
                {
                    switch (board[i][j])
                    {
                        case "Red":
                            UIboard[i][j].Fill = new SolidColorBrush(System.Windows.Media.Colors.Red);
                            break;
                        case "Black":
                            UIboard[i][j].Fill = new SolidColorBrush(System.Windows.Media.Colors.Black);
                            break;
                        default:
                            UIboard[i][j].Fill = new SolidColorBrush(System.Windows.Media.Colors.White);
                            break;
                    }
                }
            }
        }

        // creates the board for backend and front end
        private void createBoard()
        {
            // add 7 empty coloumns
            board[0] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            board[1] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            board[2] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            board[3] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            board[4] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            board[5] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };
            board[6] = new String[6] { "Empty", "Empty", "Empty", "Empty", "Empty", "Empty" };

            // set up vectors to hold board to be drawn 
            List<Ellipse> col0 = new List<Ellipse>();
            col0.Add(ZeroZero);
            col0.Add(ZeroOne);
            col0.Add(ZeroTwo);
            col0.Add(ZeroThree);
            col0.Add(ZeroFour);
            col0.Add(ZeroFive);

            // set up vectors to hold board to be drawn 
            List<Ellipse> col1 = new List<Ellipse>();
            col1.Add(OneZero);
            col1.Add(OneOne);
            col1.Add(OneTwo);
            col1.Add(OneThree);
            col1.Add(OneFour);
            col1.Add(OneFive);

            // set up vectors to hold board to be drawn 
            List<Ellipse> col2 = new List<Ellipse>();
            col2.Add(TwoZero);
            col2.Add(TwoOne);
            col2.Add(TwoTwo);
            col2.Add(TwoThree);
            col2.Add(TwoFour);
            col2.Add(TwoFive);

            // set up vectors to hold board to be drawn 
            List<Ellipse> col3 = new List<Ellipse>();
            col3.Add(ThreeZero);
            col3.Add(ThreeOne);
            col3.Add(ThreeTwo);
            col3.Add(ThreeThree);
            col3.Add(ThreeFour);
            col3.Add(ThreeFive);

            // set up vectors to hold board to be drawn 
            List<Ellipse> col4 = new List<Ellipse>();
            col4.Add(FourZero);
            col4.Add(FourOne);
            col4.Add(FourTwo);
            col4.Add(FourThree);
            col4.Add(FourFour);
            col4.Add(FourFive);

            // set up vectors to hold board to be drawn 
            List<Ellipse> col5 = new List<Ellipse>();
            col5.Add(FiveZero);
            col5.Add(FiveOne);
            col5.Add(FiveTwo);
            col5.Add(FiveThree);
            col5.Add(FiveFour);
            col5.Add(FiveFive);

            // set up vectors to hold board to be drawn 
            List<Ellipse> col6 = new List<Ellipse>();
            col6.Add(SixZero);
            col6.Add(SixOne);
            col6.Add(SixTwo);
            col6.Add(SixThree);
            col6.Add(SixFour);
            col6.Add(SixFive);

            UIboard.Add(col0);
            UIboard.Add(col1);
            UIboard.Add(col2);
            UIboard.Add(col3);
            UIboard.Add(col4);
            UIboard.Add(col5);
            UIboard.Add(col6);
        }

        // resets the board
        private void playAgainButton_Click(object sender, RoutedEventArgs e)
        {
            Menu mw = new Menu();
            mw.Show();
            this.Close();
        }
    }
}
